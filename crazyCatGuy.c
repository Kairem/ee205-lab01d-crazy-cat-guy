////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - Crazy Cat Guy - EE 205 - Spr 2022
///
/// @file    crazyCatGuy.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o crazyCatGuy crazyCatGuy.c
///
/// Usage:  crazyCatGuy n
///   n:  Sum the digits from 1 to n
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ ./crazyCatGuy 6
///   The sum of the digits from 1 to 6 is 21
///
/// @author  Kai Matsusaka <kairem@hawaii.edu>
/// @date    1/19/22
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {
   int n = atoi( argv[1] ); //get number of years as a cat man.
   int total = 0;

   int i = 1;
   while(i <= n){
      total += i;
      i++;
   }

   printf("%d\n ", total);
   return 0;
}
